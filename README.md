# Liqpay for Woocommerce #
Just a few lines of code to connect payment system Liqpay to your Woocommerce Shop.

There are some tips in some-page.php (I use thankyou.php file for my pay button).

It is NOT a plugin, it's just a code fragment for using Liqpay.

## How to use: ##
1. Copy code from **some-page.php** to your file in the place where you want to have Pay Button. The best file is */woocommerce/templates/checkout/thankyou.php*
Better if you use your child theme for it.

2. Fill in all the required variables.

3. Copy **catcher.php** to your parent directory (where are wp-content and wp-admin folders).

For additional information go here: *https://www.liqpay.com/en/doc/liq_buy*