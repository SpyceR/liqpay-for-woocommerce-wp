<!-- Html/php code before order button-->
<?php
if ($order->payment_method == 'ENTER HERE'): //enter payment method which will be used for Liqpay payment(for example, 'paypal')
    $private_key = 'ENTER HERE'; 
    $amount      = number_format(floatval(preg_replace('#[^\d.]#', '', $order->get_formatted_order_total())), 2, ',', ''); //remove any non-digit symbol from payment amount
    $result_url       = 'ENTER HERE'; //URL in your store where the payer will be redirected after the payment. The maximum length is characters
    $server_url       = 'ENTER HERE'; //URL API in your store for notifications of payment status change (server->server). In my case it is www.site-url.com/catcher.php
    $id          = $order->get_order_number(); //Unique purchase id in your store.
    $json_array  = array(
        'version' => '3', //Version of the API. Value - 3
        'public_key' => 'ENTER HERE',
        'amount' => $amount,
        'currency' => 'UAH', // Payment currency.Possible values: USD, EUR, RUB, UAH
        'result_url' => $result_url,
        'server_url' => $server_url,
        'language'       => 'en', //Language of the payment page. Possible values: ru, en, uk
        'sandbox' => '1', //Enables the testing environment for developers.
        'description' => 'Order num.' . $id . '. Date: ' . current_time('mysql') . '. Thank you for purchase!',
        'order_id' => $id
    );
    $data        = base64_encode(json_encode($json_array));
    $signature   = base64_encode(sha1($private_key . $data . $private_key, 1));
?>
			<div class="liqpay-div">
				<form method="POST" accept-charset="utf-8" action="https://www.liqpay.com/api/checkout">
					<input type="hidden" name="data" value="<?= $data ?>" />
					<input type="hidden" name="signature" value="<?= $signature ?>"/>
					<input type="image" src="http://static.liqpay.com/buttons/p1ru.radius.png" name="btn_text" />
				</form>
			</div>
<?php endif; ?>
<!-- Html/php code after order button-->