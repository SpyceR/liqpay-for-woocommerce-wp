<?php
/*After order check data.txt file for information about it*/
$ds       = DIRECTORY_SEPARATOR;
$base_dir = realpath(dirname(__FILE__) . $ds . '..') . $ds;


include_once('wp-load.php');
include_once('wp-settings.php');
include_once('wp-content/plugins/woocommerce/includes/abstracts/abstract-wc-order.php');
include_once('wp-content/plugins/woocommerce/includes/class-wc-order.php');

$data_var      = 'data';
$signature_var = 'signature';

$filename = "data.txt";
$file_cr  = fopen($filename, "a");

$data        = $_POST[$data_var]; 
$signature   = $_POST[$signature_var];
$private_key = 'ENTER HERE'; //private key must match private_key
$d_data      = json_decode(base64_decode($data), true);
$order       = new WC_Order($d_data['order_id']);
if ($signature == base64_encode(sha1($private_key . $data . $private_key, 1))) { //check private key
    if ('sandbox' == $d_data['status'] || 'success' == $d_data['status']) //check status 
	{ 
        $order->update_status('processing', 'Paid'); //change status of order if it was success
    }
}

fwrite($file_cr, 'Order number: ' . $d_data['order_id'] . ' Status: ' . $d_data['status'] . ' Transaction id: ' . $d_data['transaction_id'] . ' Amount: ' . $d_data['amount'] . ' Sender number: ' . $d_data['sender_phone'] . ' Description: ' . $d_data['description'] . PHP_EOL);
fclose($file_cr);
?>